import Ember from 'ember';
var QUOTES = [
    "Sadness is also a kind of defense.",
    "One shouldn't be afraid of the humans. Well, I am not afraid of the humans, but of what is inhuman in them.",
    "Searching for what I need, and I don't even know precisely what that is, I was going from a man to a man, and I saw that all of them together have less than me who has nothing, and that I left to each of them a bit of that what I don't have and I've been searching for.",
    "When I am not desperate, I am worthless.",
    "Between the fear that something would happen and the hope that still it wouldn't, there is much more space than one thinks. On that narrow, hard, bare and dark space a lot of us spend their lives.",
    "If people would know how little brain is ruling the world, they would die of fear.",
    "Lands of great discoveries are also lands of great injustices.",
    "What can and doesn't have to be always, at the end, surrenders to something that has to be.",
    "There is no rule without revolts and conspiracies, even as there is no property without work and worry."
];

 export default Ember.Controller.extend({
  randomQuote: function() {
    return QUOTES[Math.floor(Math.random() * QUOTES.length)];
  }.property()
});
